# localfolio_test

## Initial Setup

- Add NEWSAPI_KEY to enviroment variables

- Create virtualenv in the root folder
```
    $ virtualenv venv
```

- While in the root folder, activate virtualenv
```
    $ . venv/bin/activate
```

- While in the root folder, install necessary python packages
```
    $ pip install -r requirements.txt
```

- Go to the localfolio folder and apply django migrations
```
    $ cd localfolio
    $ python manage.py migrate
```

- Article population will be performed during API calls