# Generated by Django 2.2.9 on 2020-02-02 10:55

from django.db import migrations, models


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='NewsItem',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('source_id', models.CharField(max_length=64)),
                ('source_name', models.CharField(max_length=128)),
                ('author', models.CharField(max_length=128)),
                ('title', models.TextField()),
                ('description', models.TextField()),
                ('url', models.CharField(max_length=512)),
                ('urlToImage', models.CharField(max_length=512)),
                ('publishedAt', models.DateTimeField()),
                ('content', models.TextField()),
            ],
        ),
    ]
