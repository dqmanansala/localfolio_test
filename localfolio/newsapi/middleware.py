import requests
from django.conf import settings
from datetime import datetime, timezone
from .models import NewsItem

def retrieve_news():
    NewsItem.objects.all().delete()
    api_key = settings.NEWSAPI_KEY
    news_url = f"https://newsapi.org/v2/everything?domains=bbc.co.uk,abc.net.au,wsj.com,nytimes.com&pageSize=100&sortBy=publishedAt&apiKey={api_key}"
    r = requests.get(news_url)
    if r.status_code < 400:
        json_result = r.json()
        total_results = json_result.get('totalResults',0)

        if total_results > 0:


            articles = [ NewsItem(
                source_id = f"{article['source']['id']}",
                source_name = f"{article['source']['name']}",
                author = f"{article['author']}",
                title = f"{article['title']}",
                description = f"{article['description']}",
                url = f"{article['url']}",
                urlToImage = f"{article['urlToImage']}",
                publishedAt = f"{article['publishedAt']}",
                content = f"{article['content']}"
            ) for article in json_result.get('articles',[])]

            # save all articles to the database
            NewsItem.objects.bulk_create(articles)


def news_middleware(get_response):
    # One-time configuration and initialization.

    def middleware(request):
        # Code to be executed for each request before
        # the view (and later middleware) are called.
        
        # check if NewsItem table is populated
        if NewsItem.objects.count() < 100:
            # if NewsItem has less than 100 articles 
            retrieve_news()
        else:
            latest_news_item = NewsItem.objects.all().order_by('-publishedAt')[:1].get()
            time_now = datetime.now(timezone.utc)
            duration = time_now - latest_news_item.publishedAt
            print(latest_news_item.publishedAt)
            print(time_now)
            print(duration.total_seconds())
            
            # if latest news item is more than 30min old, get new batch of articles
            if duration.total_seconds() > 1800:
                retrieve_news()

        response = get_response(request)

        # Code to be executed for each request/response after
        # the view is called.

        return response

    return middleware