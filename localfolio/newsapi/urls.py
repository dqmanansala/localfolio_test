from django.urls import path

from . import views

urlpatterns = [
    path('', views.IndexView.as_view(), name='index'),
    path('list-news/', views.NewsListView.as_view(), name='list_news')
]
