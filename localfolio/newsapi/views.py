from django.views.generic import ListView
from django.shortcuts import render
from django.http import JsonResponse, HttpResponse
from django.views import View
from django.template import loader
from .models import NewsItem


class IndexView(View):
    def get(self, request):
        news_items = NewsItem.objects.all().order_by('-publishedAt').values()
        news_items_list = list(news_items)
        return JsonResponse(news_items_list, safe=False)


class NewsListView(ListView):
    context_object_name = 'news_items_list'
    queryset = NewsItem.objects.all().order_by('-publishedAt')[:20]
    template_name = 'news_list.html'
