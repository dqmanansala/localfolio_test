from django.db import models

# Create your models here.
class NewsItem(models.Model):
    source_id = models.CharField(max_length=64)
    source_name = models.CharField(max_length=128)
    author = models.CharField(max_length=128)
    title = models.TextField()
    description = models.TextField()
    url = models.CharField(max_length=512)
    urlToImage = models.CharField(max_length=512)
    publishedAt = models.DateTimeField()
    content = models.TextField()
